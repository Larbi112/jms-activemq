package jms;

import java.util.Scanner;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;

public class Consumer {

	public static void main(String[] args) {

//		Scanner scanner = new Scanner(System.in);
//		System.out.print("Code :");
//		String code = scanner.nextLine();

		try {
			// Create a connection Factory
			ConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://localhost:61616");

			// Create a connection
			Connection connection = connectionFactory.createConnection();
			connection.start();
			// Create a session
			// Session non transactionnelle:pas besoin de faire un commit � la fin
			// pour envoyer le message (false)
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE); // accus� de reception envoy�
																							// par
																							// le consommateur
																							// :Session.AUTO_ACKNOWLEDGE

			// Create the destination :Topic or queue
			// Destination destination = session.createQueue("enset.queue"); // nom de la
			// file d'attente

			Destination destination = session.createTopic("enset.topic");
			MessageConsumer consumer = session.createConsumer(destination); // cr�er le
																									// consommateur
			consumer.setMessageListener(new MessageListener() {

				@Override
				public void onMessage(Message message) {
					if (message instanceof TextMessage) { // faire des tests pour les types de messages
						try {
							TextMessage textMessage = (TextMessage) message;

							System.out.println("R�ception du message :" + textMessage.getText());
						} catch (JMSException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

				}
			});

		} catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
